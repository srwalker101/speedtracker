#!/usr/bin/env python


import jinja2
import pathlib
import argparse


def render_template(env, template_name, **kwargs):
    output_path = template_name.replace(".j2", "")
    template = env.get_template(template_name)

    text = template.render(**kwargs)
    with open(output_path, "w") as outfile:
        outfile.write(text)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--python", required=True)
    parser.add_argument("--script", required=True)
    parser.add_argument("--time", required=True)
    args = parser.parse_args()

    loader = jinja2.FileSystemLoader(".")
    environment = jinja2.Environment(loader=loader)
    destination = pathlib.Path("/etc/systemd/system")

    render_template(
        environment,
        "speedtracker.service.j2",
        python_path=args.python,
        script_path=args.script,
    )

    render_template(environment, "speedtracker.timer.j2", timing=args.time)
