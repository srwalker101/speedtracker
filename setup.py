from setuptools import setup, find_packages

setup(
    name="speedtracker",
    author="Simon Walker",
    author_email="s.r.walker101@googlemail.com",
    version="0.1.0",
    packages=find_packages(),
    install_requires=["influxdb", "speedtest-cli"],
    entry_points = {
        'console_scripts': ['speedtracker=speedtracker:main'],
    }
)
