#!/usr/bin/env python


import argparse
from contextlib import contextmanager
import speedtest
import datetime
import logging
from influxdb import InfluxDBClient

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger("speedtrack")


def fetch_results():
    logger.info("Fetching speedtest results")
    s = speedtest.Speedtest()
    logger.info("Getting best server")
    s.get_servers([])
    s.get_best_server()
    logger.info("Testing download")
    s.download()
    logger.info("Testing upload")
    s.upload()
    return s.results.dict()


def main():
    description = """
    Record speedtest results to InfluxDB
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "--hostname",
        required=False,
        default="localhost",
        help="Host that InfluxDB is running on",
    )
    parser.add_argument(
        "-d",
        "--database",
        required=False,
        default="speedtracker",
        help="Name of the InfluxDB database to post to",
    )
    parser.add_argument(
        "-v", "--verbose", action="count", help="Add more verbose logging"
    )
    args = parser.parse_args()

    if args.verbose is not None:
        if args.verbose == 1:
            logger.setLevel(logging.INFO)
        elif args.verbose > 1:
            logger.setLevel(logging.DEBUG)

    client = InfluxDBClient(host=args.hostname, database=args.database)
    client.create_database(args.database)

    results = fetch_results()

    json_body = [
        {
            "measurement": "upload",
            "tags": {
                "client_ip": results["client"]["ip"],
                "server_host": results["server"]["host"],
                "server_id": results["server"]["id"],
                "server_name": results["server"]["name"],
                "server_sponsor": results["server"]["sponsor"],
                "server_url": results["server"]["url"],
            },
            "time": results["timestamp"],
            "fields": {
                "ping": results["ping"],
                "speed": results["upload"],
                "bytes": results["bytes_sent"],
            },
        },
        {
            "measurement": "download",
            "tags": {
                "client_ip": results["client"]["ip"],
                "server_host": results["server"]["host"],
                "server_id": results["server"]["id"],
                "server_name": results["server"]["name"],
                "server_sponsor": results["server"]["sponsor"],
                "server_url": results["server"]["url"],
            },
            "time": results["timestamp"],
            "fields": {
                "ping": results["ping"],
                "speed": results["download"],
                "bytes": results["bytes_received"],
            },
        },
    ]

    client.write_points(json_body)


if __name__ == "__main__":
    main()
